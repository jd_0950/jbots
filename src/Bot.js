const tmi = require('tmi.js');
const Currency = require('./Currency.js');
const money = require('./CurrencyConfig.js');
function arrayObjectIndexOf(myArray, username, property) {
    for(var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === username) return i;
    }
    return -1;
}
class Bot {
	constructor(options, prefix, modPrefix, botName) {
		this.validate(options);
		this.validateOptions(options);
		this.options = options;
		this.client = new tmi.client(this.options);
		this.prefix = prefix;
		this.modPrefix = modPrefix;
		this.payoutList = [];
		this.botName = botName;
	}
	validateOptions(options) {
		if (options.channels) {
			if (options.channels.length > 1) {
				throw new Error("Sowwy, you cannot have more than one channel connected! This feature is coming soon...");
			}
		} else {
			throw new Error("Bro, add your channel. Check docs if you don't understand!");
		}
	}
	get Options() {
		return this.options;
	}
    validate(obj) {
        if (Object.prototype.toString.call(obj) !== '[object Object]') {
            throw new Error('Passing data should be a literal object');
        } else {
        	return true;
        }
    }    
    addViewerToDb() {
		this.client.on("join", function (channel, username, self) {
			if (self) return;
		  	money.newViewer(username);
		});    	
    }
    // commands
   	watchFor(command, res, prefix) {
		this.client.on("chat", (channel, user, message, self) => {
			if (message == this.prefix + command || message == prefix + command) {
				return this.client.say(channel, res);
			}
		});
   	}
   	viewViewerCurrency(command, res) {
   		this.client.on("chat", (channel, user, message, self) => {
   			if (self) return;
   			if (message == this.prefix + command) {
   				const username = user.username;
   				money.getSpecificViewAmount(username, (err, data) => {
   					if (data == null) {
   						return console.log("Viewer is null. Probably not processed yet. Please hold on and it should fix itself!");
   					}
   					let str = res.replace('`USER`', username).replace('`AMOUNT`', data.amount);
   					return this.client.say(channel, str)
   				});
   			}
   		});
   	}

   	modOnly(command, res) {
   		this.client.on("chat", (channel, user, message, self) => {
			if (message == this.modPrefix + command && user.mod || message == this.prefix + command && user.mod) {
				this.client.say(channel, res);
			} 		
   		});
   	}
   	broadcasterOnly(command, res) {
   		this.client.on("chat", (channel, user, message, self) => {
	   			if (this._showSendersName == false) {
					if (self) return
				}
   				if (message == this.prefix + command && user.badges.broadcaster == 1) {
   					return this.client.say(channel, res);
   				}
   		});
   	}
   	currencyPayoutListener() {
   		this.client.on("chat", (channel, user, message, self) => {
   			money.userExists(user.username, (err, viewer) => {
   				if (viewer == null) {
            if (self) return;
				  	return console.log(`${user.username} is not processed and cannot be added to the payout list. Please notify them`);
			   	}
			   	if (err) {
			   		throw new Error("Uh oh, don't know what happened there. Tweet @JD_0950");
			   	}
			 	if (!viewer) {
			   		return;
			   	} else {
			   		if (this.payoutList.indexOf(viewer.twitchUsername) >= 0) {
					   return;
				    } else {
					   this.payoutList.push(user.username);
					   return console.log(`Successfully added ${user.username} to the payout list.`);
				}  
   			}
     	});
   	});
  	}
   	payViewers() {
   		money.Payout(this.payoutList, (err, list) => {
   			if (err) {
   				throw new Error("Dayum, ran into an issue. Tweet @JD_0950 and see what he's got to say.");
   			}
   			return console.log(list);
   		});
   	}
   	connect() {
   		money.connectDb();
		return this.client.connect();
   	}

}
module.exports = Bot;