const Currency = require('./Currency.js');
const data = require('../config/config.json');
const money = new Currency({
	payout: data.currency.payout,
	startingAmount: data.currency.startingAmount,
	name: data.currency.currencyName
// Database section
}, {
	url: data.db.url,
	name: data.db.dbName
});
module.exports = money;