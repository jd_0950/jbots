const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const currencySchema = new Schema({
	twitchUsername: {
		type: String
	},
	amount: {
		type: Number
	}
});
var CurrencySchema = mongoose.model('Currency', currencySchema);
module.exports = CurrencySchema;