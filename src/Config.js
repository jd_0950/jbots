const data = require('../config/config.json');
// Configuration for bot
const config = {
	oauth: {
		username: data.oauth.OAUTH_USERNAME,
		password: data.oauth.OAUTH_PASS
	}
}
class Config {
	static get OAuthPass() {
		return config.oauth.password
	}
	static get OAuthUsername() {
		return config.oauth.username
	}
}
module.exports = Config;