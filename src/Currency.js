const mongoose = require('mongoose');
const CurrencySchema = require('./CurrencySchema.js');
class Currency {
	constructor(options, db) {
		this.payout = options.payout,
		this.startingAmount = options.startingAmount,
		this.db = db;
		this.dbName = db.name;
	}
	newViewer(username) {
		CurrencySchema.findOne({twitchUsername: username}, (err, user) => {
			if (err) {
				throw new Error("Uh oh, something went wrong for newViewers. Perhaps the user might not exist");
			}
			if (!user) {
				console.log("Added " + username + " to the DB");
				let data = {
					twitchUsername: username,
					amount: this.startingAmount
				};
				CurrencySchema.create(data, (err, user) => {
					if (err) {
						throw new Error("Damn bro, looks like we ran into an issue. OH MY GOD!!!!!!!! (Techy stuff: Error while creating new user to the db)");
					}
					return console.log("Successfully added and gave starting amount");
				});
			}
			return;
		});
	}
	getSpecificViewAmount(username, callback) {
		CurrencySchema.findOne({twitchUsername: username}, (err, viewer) => {
			if (err) {
				return callback(err);
			}
			if (!viewer) {
				return callback(null);
			}
			console.log("Retrieved: " + viewer.twitchUsername);
			return callback(null, viewer);
		});
	}
	Payout(arr, callback) {
		arr.forEach((i) => {
			// return console.log(i);
			CurrencySchema.findOne({twitchUsername: i}, (err, viewer) => {
				if (err) {
					return callback(err);
				}
				const newData = {
					amount: viewer.amount + this.payout
				};
				viewer.update(newData, (err, done) => {
					if (err) return callback(err);
					return callback(null, done);
				});
			});
		});
		// for(let i = 0; i < arr.length; i++) {
		// 	CurrencySchema.findOne({twitchUsername: arr[i]}, (err, data) => {
		// 		if (err) {
		// 			return callback(err);
		// 		}
		// 		if (!data) {
		// 			return;
		// 		}
		// 		return console.log(newData);
		// 	});
		// }
	}
	userExists(user, callback) {
		CurrencySchema.findOne({twitchUsername: user}, (err, viewer) => {
			if (err) {
				return callback(err);
			}
			return callback(null, viewer);
		});
	}
	connectDb() {
		mongoose.connect(`${this.db.url}${this.dbName}`, (err) => {
			if (err) {
				throw new Error("Uh oh! DB connection wasn't established. Let's try this again Kappa.");
			}
			return console.log("Connected to " + this.dbName)
		})
	}
}
module.exports = Currency;