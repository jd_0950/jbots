'use strict';
const tmi = require('tmi.js');
const Config = require('./src/Config.js');
const Bot = require("./src/Bot.js");
const Currency = require('./src/Currency.js');
const bot = new Bot({
    options: {
        debug: true
    },
    connection: {
        reconnect: true
    },
    identity: {
        username: Config.OAuthUsername,
        password: Config.OAuthPass
    },
    channels: ["jd_0950"]
}, "!", "-", Config.OAuthUsername);
// Bot command listeners
bot.watchFor("twitter", "Follow me on Twitter @jd_0950");
bot.modOnly('mod', "Cool shit ey?")

// Currency commands
bot.viewViewerCurrency('jcoins', '@`USER` has `AMOUNT` jcoins');

bot.currencyPayoutListener();
setInterval(function() {
    bot.payViewers();
}, 600000);
bot.addViewerToDb();
bot.connect();
